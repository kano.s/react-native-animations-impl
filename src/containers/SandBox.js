import * as React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  Animated,
  TouchableOpacity,
} from 'react-native';
import Donut from '../conponents/Donut';

const {width} = Dimensions.get('window');

export default function HomeScreen(props) {
  const animatedValue = React.useRef(new Animated.Value(0)).current;

  const interpolateWidth = animatedValue.interpolate({
    inputRange: [0, 0.5, 1],
    outputRange: [180, 120, 60],
  });

  const interpolateOpacity = animatedValue.interpolate({
    inputRange: [0, 0.79, 1],
    outputRange: [1, 1, 0],
  });

  const interpolateOpacity2 = animatedValue.interpolate({
    inputRange: [0, 0.79, 1],
    outputRange: [0, 0, 1],
  });

  const interpolateOpacity3 = animatedValue.interpolate({
    inputRange: [0, 0.5, 1],
    outputRange: [1, 1, 0],
  });

  const animate1 = () =>
    Animated.timing(animatedValue, {
      toValue: 1,
      duration: 300,
      useNativeDriver: false,
    });

  const animate2 = () =>
    Animated.timing(animatedValue, {
      toValue: 0,
      duration: 300,
      delay: 1000,
      useNativeDriver: false,
    });

  const onPressSubmit = () => {
    Animated.sequence([animate1(), animate2()]).start();
    // animate1().start();
  };

  return (
    <View style={styles.container}>
      <TouchableOpacity onPress={onPressSubmit}>
        <Animated.View
          style={[
            styles.box1,
            {width: interpolateWidth, opacity: interpolateOpacity},
          ]}>
          <Animated.Text
            style={[styles.buttonText, {opacity: interpolateOpacity3}]}>
            S u b m i t
          </Animated.Text>
        </Animated.View>
        <Donut
          style={[styles.donut, {opacity: interpolateOpacity2}]}
          color={myColors.lightGreen}/>
      </TouchableOpacity>
    </View>
  );
}

const myColors = {
  lightGreen: '#1ECD97',
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  box1: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'transparent',
    // width: 180,
    height: 60,
    borderRadius: 40,
    borderWidth: 3,
    borderColor: myColors.lightGreen,
    position: 'relative',
  },
  buttonText: {
    color: myColors.lightGreen,
    fontSize: 15,
    fontFamily: 'Arial',
    fontWeight: 'bold',
  },
  donut: {
    position: 'absolute',
    top: -1,
  },
});
