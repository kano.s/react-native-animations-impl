import * as React from 'react';
import {Button, View, Text, StyleSheet} from 'react-native';

export default function HomeScreen(props) {
  return (
    <View style={styles.container}>
      <Text>株式会社インプル</Text>
      <Button
        title="Go to Details"
        onPress={() => props.navigation.navigate('SpreadCircle')}
      />
      <Button
        title="Go to SandBox"
        onPress={() => props.navigation.navigate('SandBox')}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
