import React from 'react';
import { View, Animated } from 'react-native';
import Svg, { Circle, G } from 'react-native-svg';

const AnimatedCircle = Animated.createAnimatedComponent(Circle);

export default function Donut({
  percentage = 55,
  radius = 31,
  strokeWidth = 4,
  duration = 500,
  color = 'tomato',
  delay = 0,
  max = 100,
  textColor,
  style,
}) {
  const AnimatedValue = React.useRef(new Animated.Value(0)).current;
  const circleRef = React.useRef();
  const halfCircle = radius + strokeWidth;
  const circleCircumference = 2 * Math.PI * radius;
  
  const animation = (toValue) => {
    return Animated.timing(AnimatedValue, {
      toValue,
      duration,
      delay,
      useNativeDriver: true,
    }).start();
  };

  React.useEffect(() => {
    animation(percentage);
    AnimatedValue.addListener((v) => {
      if (circleRef?.current) {
        const maxPercentage = (100 * v.value) / max;
        const strokeDashoffset = circleCircumference - ((circleCircumference * maxPercentage) / 100);

        circleRef.current.setNativeProps({
          strokeDashoffset,
        });
      }
    });
  });

  return (
    <Animated.View style={style}>
      <Svg
        width={radius * 2}
        height={radius * 2}
        viewBox={`0 0 ${halfCircle * 2} ${halfCircle * 2}`}
      >
        <G rotation="-90" origin={`${halfCircle}, ${halfCircle}`}>
          <Circle
            cx="50%"
            cy="50%"
            stroke={color}
            strokeWidth={strokeWidth}
            r={radius}
            fill="transparent"
            strokeOpacity={0.2}
          />
          <AnimatedCircle
            ref={circleRef}
            cx="50%"
            cy="50%"
            stroke={color}
            strokeWidth={strokeWidth}
            r={radius}
            fill="transparent"
            strokeDasharray={circleCircumference}
            strokeDashoffset={circleCircumference}
            strokeLinecap="round"
          />
        </G>
      </Svg>
    </Animated.View>
  );
}
