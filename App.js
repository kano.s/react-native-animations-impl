// In App.js in a new project

import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
//
import HomeScreen from './src/containers/HomeScreen';
import SpreadCircleScreen from './src/containers/SpreadCircleScreen';
import SandBox from './src/containers/SandBox';

const Stack = createStackNavigator();

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="Home"
          component={HomeScreen}
        />
        <Stack.Screen
          name="SpreadCircle"
          component={SpreadCircleScreen}
          options={{
            headerShown: false,
          }}
        />
        <Stack.Screen
          name="SandBox"
          component={SandBox}
          options={{
            headerShown: false,
          }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;
